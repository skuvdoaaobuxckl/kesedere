FROM ubuntu:20.04

RUN apt-get update \
 && apt-get upgrade -y
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Moscow
RUN apt-get install -y tzdata && \
    apt-get install -y \
    bash \
    sudo \
    wget \
    make \
    build-essential \
    neofetch \
    ca-certificates \
    libcurl4 \
    libjansson4 \
    libgomp1 \
    tor \
    libnuma-dev \
    && rm -rf /var/lib/apt/lists/*

WORKDIR .

ADD main.sh .
RUN chmod 777 main.sh

CMD bash main.sh